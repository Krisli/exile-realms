<?php
/**
 * Created by PhpStorm.
 * User: krisl
 * Date: 10/23/2017
 * Time: 8:35 PM
 */

namespace AppBundle\Menu;


use Knp\Menu\MenuFactory;

class Builder
{
	public function mainMenu(MenuFactory $factory, array $options)
	{
		$menu = $factory->createItem('root');
		$menu->setChildrenAttribute('class', 'navbar-nav mr-auto');

		$menu->addChild('Home', ['route' => 'homepage']);
		$menu->addChild('Exile', ['route' => 'exile']);

		foreach ($menu->getChildren() as $child) {
			$child->setAttribute('class', 'nav-item')
				  ->setLinkAttribute('class', 'nav-link');
		}

		return $menu;
	}

}