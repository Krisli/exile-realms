<?php

namespace ExileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personage
 *
 * @ORM\Table(name="personage")
 * @ORM\Entity(repositoryClass="ExileBundle\Repository\PersonageRepository")
 */
class Personage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var bool
     *
     * @ORM\Column(name="gender", type="boolean")
     */
    private $gender;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="hunger", type="smallint")
     */
    private $hunger;

    /**
     * @var int
     *
     * @ORM\Column(name="health", type="smallint")
     */
    private $health;

    /**
     * @var int
     *
     * @ORM\Column(name="morale", type="smallint")
     */
    private $morale;

    /**
     * @var int
     *
     * @ORM\Column(name="faith", type="smallint")
     */
    private $faith;

    /**
     * @var int
     *
     * @ORM\Column(name="strength", type="smallint")
     */
    private $strength;

    /**
     * @var int
     *
     * @ORM\Column(name="endurance", type="smallint")
     */
    private $endurance;

    /**
     * @var int
     *
     * @ORM\Column(name="intelligence", type="smallint")
     */
    private $intelligence;

    /**
     * @var int
     *
     * @ORM\Column(name="agility", type="smallint")
     */
    private $agility;

    /**
     * @var int
     *
     * @ORM\Column(name="charisma", type="smallint")
     */
    private $charisma;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float")
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float")
     */
    private $latitude;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="ExileBundle\Entity\User", inversedBy="personages")
	 */
	private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Personage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Personage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Personage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Personage
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set gender
     *
     * @param boolean $gender
     *
     * @return Personage
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return bool
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Personage
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set hunger
     *
     * @param integer $hunger
     *
     * @return Personage
     */
    public function setHunger($hunger)
    {
        $this->hunger = $hunger;

        return $this;
    }

    /**
     * Get hunger
     *
     * @return int
     */
    public function getHunger()
    {
        return $this->hunger;
    }

    /**
     * Set health
     *
     * @param integer $health
     *
     * @return Personage
     */
    public function setHealth($health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return int
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * Set morale
     *
     * @param integer $morale
     *
     * @return Personage
     */
    public function setMorale($morale)
    {
        $this->morale = $morale;

        return $this;
    }

    /**
     * Get morale
     *
     * @return int
     */
    public function getMorale()
    {
        return $this->morale;
    }

    /**
     * Set faith
     *
     * @param integer $faith
     *
     * @return Personage
     */
    public function setFaith($faith)
    {
        $this->faith = $faith;

        return $this;
    }

    /**
     * Get faith
     *
     * @return int
     */
    public function getFaith()
    {
        return $this->faith;
    }

    /**
     * Set strength
     *
     * @param integer $strength
     *
     * @return Personage
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * Get strength
     *
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * Set endurance
     *
     * @param integer $endurance
     *
     * @return Personage
     */
    public function setEndurance($endurance)
    {
        $this->endurance = $endurance;

        return $this;
    }

    /**
     * Get endurance
     *
     * @return int
     */
    public function getEndurance()
    {
        return $this->endurance;
    }

    /**
     * Set intelligence
     *
     * @param integer $intelligence
     *
     * @return Personage
     */
    public function setIntelligence($intelligence)
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    /**
     * Get intelligence
     *
     * @return int
     */
    public function getIntelligence()
    {
        return $this->intelligence;
    }

    /**
     * Set agility
     *
     * @param integer $agility
     *
     * @return Personage
     */
    public function setAgility($agility)
    {
        $this->agility = $agility;

        return $this;
    }

    /**
     * Get agility
     *
     * @return int
     */
    public function getAgility()
    {
        return $this->agility;
    }

    /**
     * Set charisma
     *
     * @param integer $charisma
     *
     * @return Personage
     */
    public function setCharisma($charisma)
    {
        $this->charisma = $charisma;

        return $this;
    }

    /**
     * Get charisma
     *
     * @return int
     */
    public function getCharisma()
    {
        return $this->charisma;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Personage
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Personage
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set user
     *
     * @param \ExileBundle\Entity\User $user
     *
     * @return Personage
     */
    public function setUser(\ExileBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ExileBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
