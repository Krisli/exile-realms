<?php
/**
 * Created by PhpStorm.
 * User: krisl
 * Date: 10/26/2017
 * Time: 12:10 AM
 */

namespace ExileBundle\Controller;


use ExileBundle\Entity\User;
use ExileBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller
{
	/**
	 * @Route("/register", name="user_registration")
	 *
	 * @param Request                      $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function registerAction(Request $request)
	{
		$passwordEncoder = $this->get('security.password_encoder');
		$user = new User();
		$form = $this->createForm(UserType::class, $user);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
		 	$password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
		 	$user->setPassword($password);

			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}

			$ip = ip2long($ip);
			$user->setIP($ip);

		 	$em = $this->getDoctrine()->getManager();
		 	$em->persist($user);
		 	$em->flush();

		 	$this->addFlash(
		 		'notice',
			    'Registration complete!'
		    );
		 	return $this->redirect('homepage');
		}

		$this->addFlash(
			'warning',
			'There was an error during the registration!'
		);

		return $this->render('@Exile/Default/index.html.twig', array(
			'form' => $form->createView()
		));
	}
}