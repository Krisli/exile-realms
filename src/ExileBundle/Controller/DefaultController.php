<?php

namespace ExileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="exile")
     */
    public function indexAction()
    {
    	$form = $this->createFormBuilder()->setAction($this->generateUrl('user_registration'))
		    ->add('email', EmailType::class)
		    ->add('plainPassword', RepeatedType::class, array(
			    'type' => PasswordType::class,
			    'first_options' => array('label' => 'Password'),
			    'second_options' => array('label' => 'Repeat password')
		    ))
		    ->getForm();


	    return $this->render('ExileBundle:Default:index.html.twig', array(
        	'form' => $form->createView()
        ));
    }
}
